# The problem

## Ingredients

* LetsEncrypt certbot 
* Ubuntu linux 
* Apache with vhost configurations
* Using the certbot default http challenge method
* All software current as of June 2023

You run certbot and get "Challenge failed for domain blah.blah.blah.com"

Ex. 

```bash
sudo certbot --apache --test-cert --debug-challenges -v
```
You select your site

Ex. 
blah.blah.blah.com

And then the script continues:

```
Challenges loaded. Press continue to submit to CA.

The following URLs should be accessible from the internet and return the value
mentioned:

URL:
http://blah.blah.blah.com/.well-known/acme-challenge/HkPGD22eaOc1JIkPmMKcYu-21CMo6az5e3Cg9svVDlU
Expected value:
HkPGD22eaOc1JIkPmMKcYu-21CMo6az5e3Cg9svVDlU.Vsx1eKmKnLSZ32Mc42Ny5aY4QvTBKxasNee8y8lFCx0
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue
Waiting for verification...
Challenge failed for domain blah.blah.blah.com
http-01 challenge for blah.blah.blah.com

Certbot failed to authenticate some domains (authenticator: apache). The Certificate Authority reported these problems:
  Domain: blah.blah.blah.com
  Type:   unauthorized
  Detail: 192.168.45.214: Invalid response from http://blah.blah.blah.com/.well-known/acme-challenge/HkPGD22eaOc1JIkPmMKcYu-21CMo
az5e3Cg9svVDlU: 403

Hint: The Certificate Authority failed to verify the temporary Apache configuration changes made by Certbot. Ensure that the listed domains point to this Apache server and that it is accessible from the internet.

Cleaning up challenges
Some challenges have failed.
```

If you see output like the above, try the steps below.


# The solution

## ServerAdmin

First, make sure the virtual host has a **ServerAdmin** entry. Place it at the top of the section.

```
<VirtualHost *:80>
  ServerAdmin webmaster@localhost

  ServerName blah.blah.blah.com
  ServerAlias wullaloooo.com
```  

Certbot locates an include directive that contains a rewrite rule using the **ServerAdmin** directive in your virtual host section. The rewrite rule looks like:

```
  RewriteEngine on
  RewriteRule ^/\.well-known/acme-challenge/([A-Za-z0-9-_=]+)$ /var/lib/letsencrypt/http_challenges/$1 [END]
```  

If the **ServerAdmin** directive is not present, the include (and rewrite rule) aren't added which means the challenge key won't be findable.

It might go without saying, but since it's using a rewrite, also make sure the apache rewrite module is installed and enabled.

At the end of the virtual host section, certbot adds another include. This one contains directory and location settings.

```
  <Directory /var/lib/letsencrypt/http_challenges>
      Require all granted
  </Directory>
  <Location /.well-known/acme-challenge>
      Require all granted
  </Location>
```

Since it uses the end of the section to add this include, there probably won't be an issue here. But it is informative to know the directory and location that is being configured.  

## Verify the HTTP challenges folder

Check your apache error logs. If you see entries like

```
access to /.well-known/acme-challenge/VyJqJNAqhiiCULnMqVZe-u49LhLWYw3EkyugItZe0wo denied
(filesystem path '/var/lib/letsencrypt/http_challenges') because search permissions are 
missing on a component of the path
```

that means the path to the http challenges has incorrect permissions.

The http challenges are written by certbot to `/var/lib/letsencrypt/http_challenges`.

Check each folder in the path. However, the problem is likely the last two folders:

```bash
ll /var/lib/letsencrypt

drwx------  4 root root 4096 Jun 21 23:46 ./
drwxr-xr-x 48 root root 4096 Jun 21 17:14 ../
drwx------  4 root root 4096 Jun 21 23:46 backups/
drwx------  2 root root 4096 Jun 21 23:46 http_challenges/
```

If you see permissions like this, then that is the reason apache is telling you that search permissions are missing.

Fix it with

```bash
sudo chmod go+rx /var/lib/letsencrypt
sudo chmod go+rx /var/lib/letsencrypt/http_challenges

ll /var/lib/letsencrypt

drwxr-xr-x  4 root root 4096 Jun 21 23:46 ./
drwxr-xr-x 48 root root 4096 Jun 21 17:14 ../
drwx------  4 root root 4096 Jun 21 23:46 backups/
drwxr-xr-x  2 root root 4096 Jun 21 23:46 http_challenges/
```

Note that this means certbot is **not** writing challenge keys to `<docroot>/.well-known/http_challenge`. Do not let content in that folder confuse you. Certbot is generating the challenge keys and writing them to `/var/lib/letsencrypt/https_challenges`. And then it uses the rewrite rule

```
RewriteRule ^/\.well-known/acme-challenge/([A-Za-z0-9-_=]+)$ /var/lib/letsencrypt/http_challenges/$1 [END]
```

to allow the challenges to be found as if they were at `<docroot>/.well-known/http_challenge`.

## LetsEncrypt log

Also, check the certbot log at `/var/log/letsencrypt/letsencrypt.log`. Reviewing that will likely be easier than scrolling back through the output from -v.


## That's it!

That's it. Hopefully these suggestions helped your resolve your LetsEncrypt certbot http challenge issues.

## Followup

23 June 2023

This issue may have been introduced by installing certbot using a command like:

`sudo apt install certbot python3-certbot-apache`

On a fresh box using the instructions for ubuntu 20 and apache from letsencrypt which are

```
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

certbot worked correctly.


